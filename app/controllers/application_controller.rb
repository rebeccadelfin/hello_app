class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  def hello
    render text: "hello, world!"
  end
  
  def starwars
    render text: "in a galaxy far, far away..."
  end
  
  def wsup
    render text: "wsup guille"
  end
  
end
